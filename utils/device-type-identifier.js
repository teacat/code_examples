class DeviceTypeIdentifier_C {
    pageType_c = null;
    constructor() {
        // as singleton
        if (typeof DeviceTypeIdentifier_C.instance === 'object') {
            return DeviceTypeIdentifier_C.instance;
        }
        DeviceTypeIdentifier_C.instance = this;
        return DeviceTypeIdentifier_C.instance;
    }

    static getPageTypeKey() {
        const strategies = {
            "XLG-MAX": (pageSize) => pageSize > 1600,
            "XLG": (pageSize) => pageSize <= 1600 && pageSize > 1366,
            "LG": (pageSize) => pageSize <= 1366 && pageSize > 1280,
            "MD": (pageSize) => pageSize <= 1280 && pageSize > 1024,
            "TABLET_HOR": (pageSize) => pageSize <= 1024 && pageSize > 768,
            "SM": (pageSize) => pageSize <= 768 && pageSize > 480,
            "XS": (pageSize) => pageSize <= 480,
        }
        const currentPageWidth = window.innerWidth || document.body.getBoundingClientRect().width;
        let result = null;
        for (let key in strategies) {
            result = strategies[key](currentPageWidth) && key;
            if (result) break
        }
        return result;
    }

    static determinatePageType() {
        const newPageType = DeviceTypeIdentifier_C.getPageTypeKey();
        if (this.pageType_c !== newPageType) this.pageType_c = newPageType;
        return this.pageType_c;
    }

    addPageTypeWatcher_P() {
        this.pageType_c = DeviceTypeIdentifier_C.determinatePageType();
        window.addEventListener('resize', () => {
            this.pageType_c = DeviceTypeIdentifier_C.determinatePageType();
        });
    }
}

const DeviceTypeIdentifier = new DeviceTypeIdentifier_C();

export default DeviceTypeIdentifier;
