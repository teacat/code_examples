export const bidActions = {
    increase_by_percentage: 'Increase by percentage',
    decrease_by_percentage: 'Decrease by percentage',
    increase_by_value: 'Increase by value',
    decrease_by_value: 'Decrease by value',
    set_value: 'Set value'
};

export const stateActions = {
    enabled: 'Enabled',
    paused: 'Paused',
    archived: 'Archived',
};

export const rangeValues = [
    { value: 0, title: 'Period'},
    { value: 1, title: 'Today'},
    { value: 3, title: 'Last 3 days'},
    { value: 7, title: 'Last week'},
    { value: 14, title: 'Last 2 weeks'},
    { value: 30, title: 'Last 30 days'},
    { value: 60, title: 'Last 60 days'},
    { value: 90, title: 'Last 90 days'},
    { value: 120, title: 'Last 120 days'}
];
