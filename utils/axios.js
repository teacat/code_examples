import axios from 'axios';
import { getApiBaseUrl } from './domain';
import StackdriverErrorReporter from 'stackdriver-errors-js';

const LOGGING_API_KEY = 'XXXX';
const LOGGING_PROJECT_ID = 'linear-stock-330017';
const isDev = process.env.NODE_ENV === 'development';
const apiUrlDev = 'https://api.xxxx.io/';
const apiUrl = getApiBaseUrl();

let config = {
    baseURL: isDev ? apiUrlDev : apiUrl,
    headers: {},
    withCredentials: true
};

const http = axios.create(config);

const errorHandler = new StackdriverErrorReporter();
errorHandler.start({
    key: LOGGING_API_KEY,
    projectId: LOGGING_PROJECT_ID,
});

const authInterceptor = config => {
    config.auth = isDev ? { username: 'xxx@xxx.xx', password: 'xxxx' } : {};

    config.headers.Authorization = isDev ? 'Basic ' + `${window.btoa('xxx@xxx.xx:xxxx')}` : {};

    return config;
};

const loggerInterceptor = config => {
    /** как-то обрабатываем логи */
    return config;
}

/** Добавляем экземпляру созданные обработчики для аутентификации и логов */
http.interceptors.request.use(authInterceptor);
http.interceptors.request.use(loggerInterceptor);

/** Добавление обработчика для результатов или ошибок при запросах */
http.interceptors.response.use(
    response => {
        /** Как-то обрабатываем успешный результат */
        return response;
    },

    error => {
        /** Как-то обрабатываем результат, завершенный ошибкой */
        errorHandler.report(error);
        return Promise.reject(error);
    }
);

export { http };
