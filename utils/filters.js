import Vue from 'vue';
import * as dayjs from 'dayjs';

const toPrice = Vue.filter('toPrice', function (value) {
  if (!value) return '';
  return Math.floor(value).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
});

const toPlural = Vue.filter('toPlural', function(value, one, two, five, isShowValue = true) {
  let n = Math.abs(value);
  n %= 100;

  const quantity = isShowValue ? value : '';

  if (n >= 5 && n <= 20) {
    return `${quantity} ${five}`;
  }

  n %= 10;
  if (n === 1) {
    return `${quantity} ${one}`;
  }

  if (n >= 2 && n <= 4) {
    return `${quantity} ${two}`;
  }

  return `${quantity} ${five}`;
});

const toPhoneFormat = Vue.filter('toPhoneFormat', function (value) {
  if (!value || value.length !== 11) return value;
  return `+${value.charAt(0)} (${value.slice(1, 4)}) ${value.slice(4, 7)}-${value.slice(7, 9)}-${value.slice(9, 11)}`;
});

const decodeHtml = Vue.filter('decodeHtml' , function (str) {
  const chars =
      {
        '&amp;': '&',
        '&lt;': '<',
        '&gt;': '>',
        '&quot;': '"',
        '&#039;': "'"
      };
  return str.replace(/&amp;|&lt;|&gt;|&quot;|&#039;/g, c => chars[c]);
});

const dateFormat = Vue.filter('dateFormat', function (date, format) {
  return date ? dayjs(date).format(format) : 'no date';
});

const cropText = Vue.filter('cropText', function(rawText, max) {
    if (rawText.trim().length <= max) return rawText.trim();

    const words = rawText.trim().split(' ');
    const newWords = [];
    let length = 0;

    for (let i = 0; (i < words.length, length < max); i += 1) {
        length += words[i].length + 1;
        newWords.push(words[i]);
    }

    let croppedText = newWords.join(' ');
    croppedText = croppedText.replace(/[.,!:;\-\\{(/#$%^&*=_`~]$/, '');
    return `${croppedText}...`;
});

const roundToHundredths = Vue.filter('roundToHundredths', function(num) {
    return num === 0 ? 0 : Number(num).toFixed(2);
});

export default {
    toPrice,
    toPlural,
    toPhoneFormat,
    decodeHtml,
    roundToHundredths
};
