const host = window.location.hostname;

export const getApiBaseUrl = () => {
    const parts = host.split('.');
    return 'https://api.' + parts[1] + '.' + parts[2] + '/';
};

export const getCookieBaseUrl = () => {
    const parts = host.split('.');
    return parts[1] + '.' + parts[2];
};
