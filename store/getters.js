export default {
  isLoading: state => state.isLoading,
  profileId: state => state.profileId,
  marketplaces: state => state.marketplaces,
  notifyList: state => state.notify.list,

  algorithmPortfolios: state => state.algorithm.portfolios,
  algorithmCampaigns: state => state.algorithm.campaigns,
  algorithmKeywords: state => state.algorithm.keywords,
  algorithmSettings: state => state.algorithm.settings,
  algorithmTriggers: state => state.algorithm.triggers
};
