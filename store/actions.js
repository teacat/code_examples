import get from 'lodash/get';
import { http } from '@/utils/axios';
import * as TYPES from './types';

export default {
  async getMarketplaces({ dispatch, commit }) {
    try {
      const response = await http.get('/marketplaces/');
      commit(TYPES.SET_MARKETPLACES, response.data);
      return response.data;
    } catch (err) {
      console.warn(err);
      dispatch('showNotify', { type: 'error', title: get(err, 'response.data.message') });
      return false;
    }
  },

  setProfileId({ commit }, payload) {
    commit(TYPES.SET_PROFILE_ID, payload);
  },

  setLoading({ commit }, payload) {
    commit(TYPES.SET_LOADING, payload);
  }
};
