import * as TYPES from './types';

export default {
  [TYPES.SET_MARKETPLACES](state, payload) {
    state.marketplaces = payload;
  },
  [TYPES.SET_PROFILE_ID](state, payload) {
    state.profileId = payload;
  },
  [TYPES.SET_LOADING](state, payload) {
    state.isLoading = payload;
  },
};
