import * as TYPES from '../types';
import { http } from '@/utils/axios';
import get from 'lodash/get';

const baseAlgorithmURL = '/automation/algorithms/';

const state = {
    campaigns: [],
    keywords: [],
    portfolios: [],
    triggers: [],
    settings: []
};

const actions = {
    async getAvailableAlgorithms({ commit, dispatch }, paramStr) {
        try {
            const params = paramStr ? paramStr : '';
            commit(TYPES.SET_LOADING, true);
            const response = await http.get(baseAlgorithmURL + 'available/' + params);
            commit(TYPES.SET_LOADING, false);
            return response.data.results;
        } catch (err) {
            console.warn(err);
            const message = get(err, 'response.data.message');
            if (message) dispatch('showNotify', { type: 'error', title: message });
            commit(TYPES.SET_LOADING, false);
            return false;
        }
    },

    async getPortfolios({ commit, dispatch }, data) {
        try {
            commit(TYPES.SET_LOADING, true);
            const response = await http.get('/portfolios/?profile_id__in=' + data.ids);
            commit(TYPES.SET_ALGORITHM_PORTFOLIOS, response.data.results);
            commit(TYPES.SET_LOADING, false);
            return response.data.results;
        } catch (err) {
            console.warn(err);
            const message = get(err, 'response.data.message');
            if (message) dispatch('showNotify', { type: 'error', title: message });
            commit(TYPES.SET_LOADING, false);
            return false;
        }
    },

    async getAlgorithmsSettings({ commit, dispatch }, data) {
        try {
            commit(TYPES.SET_LOADING, true);
            const response = await http.get(baseAlgorithmURL + 'breakpoints/?algorithm_id=' + data.id);
            commit(TYPES.SET_ALGORITHM_SETTINGS, response.data);
            commit(TYPES.SET_LOADING, false);
            return response.data;
        } catch (err) {
            console.warn(err);
            const message = get(err, 'response.data.message');
            if (message) dispatch('showNotify', { type: 'error', title: message });
            commit(TYPES.SET_LOADING, false);
            return false;
        }
    },

    async getAlgorithmsTriggers({ commit, dispatch }, data) {
        try {
            commit(TYPES.SET_LOADING, true);
            const response = await http.get(baseAlgorithmURL + 'triggers/?algorithm_id=' + data.id);
            commit(TYPES.SET_ALGORITHM_TRIGGERS, response.data);
            commit(TYPES.SET_LOADING, false);
            return response.data;
        } catch (err) {
            console.warn(err);
            const message = get(err, 'response.data.message');
            if (message) dispatch('showNotify', { type: 'error', title: message });
            commit(TYPES.SET_LOADING, false);
            return false;
        }
    },

    async createAlgorithm({ commit, dispatch }, data) {
        try {
            commit(TYPES.SET_LOADING, true);
            const response = await http.post(baseAlgorithmURL, data);
            commit(TYPES.SET_LOADING, false);
            dispatch('showNotify', { type: 'success', title: `New algorithm "${response.data.title}" was created!` });
            commit(TYPES.CLEAR_ALGORITHM_DATA);
            return response.data;
        } catch (err) {
            console.warn(err);
            const message = get(err, 'response.data.message');
            if (message) dispatch('showNotify', { type: 'error', title: message });
            commit(TYPES.SET_LOADING, false);
            return false;
        }
    },

    async updateAlgorithm({ commit, dispatch }, data) {
        try {
            commit(TYPES.SET_LOADING, true);
            const response = await http.patch(baseAlgorithmURL + data.id, data);
            commit(TYPES.SET_LOADING, false);
            dispatch('showNotify', { type: 'success', title: `Algorithm "${response.data.title}" was updated!` });
            commit(TYPES.CLEAR_ALGORITHM_DATA);
            return response.data;
        } catch (err) {
            console.warn(err);
            const message = get(err, 'response.data.message');
            if (message) dispatch('showNotify', { type: 'error', title: message });
            commit(TYPES.SET_LOADING, false);
            return false;
        }
    },

    async getAlgorithms({ commit, dispatch }) {
        try {
            commit(TYPES.SET_LOADING, true);
            const response = await http.get(baseAlgorithmURL);
            commit(TYPES.SET_LOADING, false);
            return response.data;
        } catch (err) {
            console.warn(err);
            const message = get(err, 'response.data.message');
            if (message) dispatch('showNotify', { type: 'error', title: message });
            commit(TYPES.SET_LOADING, false);
            return false;
        }
    },

    async deleteAlgorithm({ commit, dispatch }, data) {
        try {
            commit(TYPES.SET_LOADING, true);
            const response = await http.delete(baseAlgorithmURL + data.id);
            commit(TYPES.SET_LOADING, false);
            dispatch('showNotify', { type: 'success', title: 'Algorithm was deleted!' });
            return response.data;
        } catch (err) {
            console.warn(err);
            const message = get(err, 'response.data.message');
            if (message) dispatch('showNotify', { type: 'error', title: message });
            commit(TYPES.SET_LOADING, false);
            return false;
        }
    },
};

const mutations = {
    [TYPES.SET_ALGORITHM_PORTFOLIOS](state, payload) {
        state.portfolios = payload;
    },

    [TYPES.SET_ALGORITHM_CAMPAIGNS](state, payload) {
        state.campaigns = payload;
    },

    [TYPES.SET_ALGORITHM_KEYWORDS](state, payload) {
        state.keywords = payload;
    },

    [TYPES.SET_ALGORITHM_SETTINGS](state, payload) {
        state.settings = payload;
    },

    [TYPES.SET_ALGORITHM_TRIGGERS](state, payload) {
        state.triggers = payload;
    },

    [TYPES.CLEAR_ALGORITHM_DATA](state) {
        state.campaigns = [];
        state.keywords = [];
        state.portfolios = [];
        state.triggers = [];
        state.settings = [];
    }
};

export default {
    state,
    actions,
    mutations
};
