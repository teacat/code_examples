import uniqueId from 'lodash/uniqueId';
import * as TYPES from '../types';

const state = {
    list: [],
};

const actions = {
    showNotify({ commit }, payload) {
        commit(TYPES.SHOW_NOTIFY, {
            id: uniqueId('notification'),
            ...payload,
        });
    },

    removeNotify({ commit }, payload) {
        commit(TYPES.REMOVE_NOTIFY, payload);
    },
};

const mutations = {
    [TYPES.SHOW_NOTIFY](state, payload) {
        state.list.push(payload);
    },

    [TYPES.REMOVE_NOTIFY](state, payload) {
        const index = state.list.indexOf(payload);
        state.list.splice(index, 1);
    },
};

export default {
    state,
    actions,
    mutations,
};