import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/algorithms',
    component: () => import('@/views/Algorithm/Index.vue'),
    children: [
      {
        path: '',
        name: 'algorithm-list',
        component: () => import('@/views/Algorithm/List.vue'),
        meta: { searchType: 'All' },
      },
      {
        path: '/algorithms/create',
        name: 'algorithm-create',
        component: () => import('@/views/Algorithm/Create.vue'),
        meta: { searchType: 'All', withoutHeader: true },
      },
      {
        path: '/algorithms/edit/:id(\\d+)',
        name: 'algorithm-edit',
        component: () => import('@/views/Algorithm/Create.vue'),
        meta: { searchType: 'All', withoutHeader: true }
      },
    ]
  },
  {
    path: '/*',
    name: 'empty',
    component: () => import('@/views/Overview.vue'),
    meta: { searchType: 'All' }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
